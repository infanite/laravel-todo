@extends('layout')

@section('content')

    <form action="{{ route('todo.save', ['id' => $todo->id]) }}" method="post">
        @csrf
        <input type="text" class="form-control input-lg" value="{{ $todo->todo }}" name="todo">
    </form>

@endsection