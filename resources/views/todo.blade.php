@extends('layout')

@section('content')
    <form action="/todos/create" method="post">
        @csrf
        <input type="text" class="form-control input-lg" name="todo" placeholder="Enter your todos">
    </form>

    <hr>
    @foreach ($todos as $todo)
        {{ $todo->todo }} <a href="{{ route('todo.delete', ['id' => $todo->id]) }}"><button class="btn btn-danger">x</button></a>
                           <a href="{{ route('todo.update', ['id' => $todo->id]) }}"> <button class="btn btn-info">Update</button> </a>
                           
                           @if(!$todo->completed)
                                <a href="{{ route('todo.completed', ['id' => $todo->id]) }}"> <button class="btn btn-success">Mark as completed</button> </a>
                            @else 
                                <span class="text-success">Completed!</span>  
                            @endif
        <hr>
    @endforeach

@endsection