<?php

namespace App\Http\Controllers;

use App\Todo;
use Session;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index()
    {
        $todos = Todo::all();

        return view('todo', compact('todos'));
    }

    public function store(Request $request)
    {
        $todos = new Todo();
        $todos->todo = $request->todo;
        $todos->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $todo = Todo::find($id);
        $todo->delete();
        Session::flash('success', 'Your todo was deleted');

        return redirect()->back();
    }

    public function update($id)
    {
        $todo = Todo::find($id);

        return view('update', compact('todo'));
    }

    public function save(Request $request, $id)
    {
        $todo = Todo::find($id);
        $todo->todo = $request->todo;
        $todo->save();
        Session::flash('success', 'Your todo was updated');

        return redirect()->route('todos');
    }

    public function completed($id)
    {
        $todo = Todo::find($id);
        $todo->completed = 1;
        $todo->save();
        Session::flash('success', 'Your todo was mark as completed');

        return redirect()->back();
    }
}
